@echo off

REM - Wix toolset components need to be on the path

REM Service installer
candle -nologo -arch x64 ServiceInstaller.wxs
light -nologo -v -ext WixUIExtension ServiceInstaller.wixobj -o ServiceInstaller.msi

REM Module installer
candle -nologo -arch x64 ModuleInstaller.wxs
light -nologo -v -ext WixUIExtension ModuleInstaller.wixobj -o ModuleInstaller.msi
