This directory is an example of a set of installers, one for a service, and one for a module that the service uses. The module install should only be done after the service is installed.
