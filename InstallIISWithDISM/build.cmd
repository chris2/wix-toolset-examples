@echo off

REM - Wix toolset components need to be on the path

REM Service installer
candle -nologo -arch x64 MyInstaller.wxs
light -nologo -v -ext WixUIExtension -ext WixUtilExtension MyInstaller.wixobj -o MyInstaller.msi
